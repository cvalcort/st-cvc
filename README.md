# ST-cvc

My own build of ST terminal with the following patches already applied (see folder patches):

- st-alpha: add transparency to ST terminal (compositor required).
- st-keyboard-select: allows copy text from terminal using the keyboard (Ctrl-shift-scape in this build, see config.h file)
- st-newterm: allows start a new instance from in the current Dir (Ctrl-Shift-T).
- st-scrollback: allows scroll the terminal output (Shift-PgUp/Pgdn)

Installation
Please follow the instructions specified in the README file.
